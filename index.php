<?php
//Front Controller

//1. Общие настройки
//включение отображения ошибок
ini_set('display_errors', 1);
error_reporting(E_ALL);

session_start();

//2. Подключение файлов системы
//Задается корневая, базовая константа пути
define('ROOT', dirname(__FILE__));
//Подключения автозагрузчика классов
require_once (ROOT.'/components/Autoload.php');

//3. Подключение маршрута
//создание объекта класса маршрутизатора, выполнение анализа запроса
$router = new Router();
//запуск метода, осуществляет передачу управления контроллеру
$router->run();
