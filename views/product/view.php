<?php include(ROOT.'/views/layouts/header.php') ?>

<section>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="left-sidebar">
                <h2>Каталог</h2>
                   <div class="panel-group category-products">

                    <!--Вывод категорий в левой колонке -->
                    <?php foreach ($categories as $categoryItem): ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="/category/<?php echo $categoryItem['id']; ?>"
                                class="
                                    <?php if (($categoryId == $categoryItem['id']) or
                                       ($product['category_id'] == $categoryItem['id'])){
                                           echo 'active';}?>
                                ">
                                <?php echo $categoryItem['name']; ?>
                                </a>
                        </h4>
                    </div>
                </div>

                <?php endforeach; ?>

                </div>

            </div>
        </div>

        <div class="col-sm-9 padding-right">
            <div class="product-details"><!--product-details-->
                <div class="row">
                    <div class="col-sm-5">
                        <div class="view-product">
                            <img src="<?php echo Product::getImage($product['id']); ?>" alt="" />
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            <?php if($product['is_new']): ?>
                            <img src="/template/images/product-details/new.jpg" class="newarrival" alt="" />
                            <?php endif; ?>
                            <h2><?php echo $product['name']; ?></h2>
                            <p>Код товара: <?php echo $product['code'];?></p>
                            <span>
                                <span><?php echo $product['price'];?> Р.</span>
                                <label>Количество:</label>
                                <input type="number" id="amount" name="amount" min="1" max="100" value="1" />
                        
                                <button type="button" data-id="<?php echo $product['id']; ?>"
                                        class="btn btn-fefault cart add-to-cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    В корзину
                                </button>
                            </span>
                            <p>
                                <b>Наличие:</b>
                                    <?php if ($product['availability']){
                                        echo ' На складе';
                                    } else {
                                        echo ' Товар отсутствует';
                                    }?>
                            </p>
                            <p><b>Состояние:</b> Новое</p>
                            <p><b>Производитель:</b> <?php echo $product['brand'];?></p>
                        </div><!--/product-information-->
                    </div>
                </div>
                <div class="row">                                
                    <div class="col-sm-12">
                        <h5>Описание товара</h5>
                        <p><?php echo $product['description'];?></p>

                    </div>
                </div>
            </div><!--/product-details-->

        </div>
    </div>
</div>
</section>


<br/>
<br/>

<?php include(ROOT.'/views/layouts/footer.php') ?>

