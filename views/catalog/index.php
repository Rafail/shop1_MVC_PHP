<?php include(ROOT.'/views/layouts/header.php') ?>


<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Каталог</h2>
                    <div class="panel-group category-products">

                        <!--Вывод категорий в левой колонке -->
                        <?php foreach ($categories as $categoryItem): ?>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="/category/<?php echo $categoryItem['id']; ?>"><?php echo $categoryItem['name']; ?></a></h4>
                            </div>
                        </div>

                        <?php endforeach; ?>

                    </div>

                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->

                    <!--Вывод последних товаров на Главной-->
                    <h2 class="title text-center">Каталог всех товаров</h2>
                    <?php foreach ($latestProducts as $product): ?>
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                 <div class="productinfo text-center">
                            
                                <div class="productinfo_top">
                                <a href="/product/<?php echo $product['id']; ?>">
                                    <img src="<?php echo Product::getImage($product['id']); ?>" alt="" />
                                </a>
                                </div>

                                <div class="productinfo_middle">
                                <h2><?php echo $product['price']; ?> РУБ.</h2>
                                <p>
                                    <a href="/product/<?php echo $product['id']; ?>">
                                        <?php echo $product['name']; ?>
                                    </a>
                                </p>
                                </div>


                                <div class="productinfo_bottom">
                                    <a href="#" data-id="<?php echo $product['id']; ?>"
                                   class="btn btn-default add-to-cart">
                                    <i class="fa fa-shopping-cart"></i>В корзину</a>
                                </div>

                            </div>

                            <!--Если товар новый, отмечаем его-->
                            <?php if ($product['is_new']): ?>
                                <img src="/template/images/home/new.png" class="new" alt="">
                            <?php endif; ?>

                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div><!--features_items-->

            </div>
        </div>
    </div>
</section>

<?php include(ROOT.'/views/layouts/footer.php') ?>
        


