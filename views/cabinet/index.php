<?php include ROOT.'/views/layouts/header.php'; ?>

<sextion>
    <div class="container">
        <div class="row">
            
            <h1>Личный кабинет</h1>
            
            <h3>Здравствуйте, <?php echo $user['name']; ?>!</h3>
            <ul>
                <?php $userId = User::checkLogged();
        //получение информации о текущем пользователе
        $user = User::getUserById($userId);
        //если пользователь имеет роль admin, его пускает в панель администратора
        if ($user['role'] == 'admin'){
            echo '<li><a href="/admin">Админпанель</a></li>';}?>
                <li><a href="/cabinet/edit">Редактировать данные</a></li>
                <li><a href="/cabinet/history">Управление заказами</a></li>
            </ul>
        </div>
    </div>
</sextion>
<?php include ROOT.'/views/layouts/footer.php'; ?>


