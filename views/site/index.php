<?php include(ROOT.'/views/layouts/header.php') ?>

<section rel="main">
<div class="container">
<div class="row">
    <div class="col-sm-3">
        <div class="left-sidebar">
            <h2>Каталог</h2>
            <div class="panel-group category-products">

                <!--Вывод категорий в левой колонке -->
                <?php foreach ($categories as $categoryItem): ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a href="/category/<?php echo $categoryItem['id']; ?>"><?php echo $categoryItem['name']; ?></a></h4>
                    </div>
                </div>

                <?php endforeach; ?>

            </div>
        </div>
    </div>

    <div class="col-sm-9 padding-right">
        <div class="features_items"><!--features_items-->

            <!--Вывод последних товаров на Главной-->
            <h2 class="title text-center">Последние товары</h2>
            <?php foreach ($latestProducts as $product): ?>
            <div class="col-sm-4">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            
                            <div class="productinfo_top">
                            <a href="/product/<?php echo $product['id']; ?>">
                                <img src="<?php echo Product::getImage($product['id']); ?>" alt="" />
                            </a>
                            </div>
                                              
                            <div class="productinfo_middle">
                            <h2><?php echo $product['price']; ?> РУБ.</h2>
                            <p>
                                <a href="/product/<?php echo $product['id']; ?>">
                                    <?php echo $product['name']; ?>
                                </a>
                            </p>
                            </div>
                            
                            
                            <div class="productinfo_bottom">
                                <a href="#" data-id="<?php echo $product['id']; ?>"
                               class="btn btn-default add-to-cart">
                                <i class="fa fa-shopping-cart"></i>В корзину</a>
                            </div>
                            
                        </div>

                        
                            
                            <!--Если товар новый, отмечаем его-->
                            <?php if ($product['is_new']): ?>
                                <img src="/template/images/home/new.png" class="new" alt="">
                            <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

        </div><!--features_items-->

        <!--Постраничная навигация-->
        <?php echo $pagination->get(); ?>

        <!--Карусель рекомендуемых товаров-->
        <div class="recommended_items">
            <h2 class="title text-center">Рекомендуемые товары</h2>
            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <!--Вывод данных в карусель-->
                    <div class="item active"><!--item active-->
                        
                        <?php for ($i = 0; $i < ($recommendedLenght); $i++):
                            
                            //После вывода 3 блоков нужно открыть элемент <div class="item">
                            if (($i > 0) and ($i % 3 == 0)) {
                                echo '<div class="item"><!--class="item"-->';}
                                
                            //Сохранение элемента массива с данными о рекомендованном товаре
                            $recProduct = $recommendedProducts[$i]; ?>
                    
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        
                                        <div class="carousel_top">
                                            <a href="/product/<?php echo $recProduct['id']; ?>">
                                                <img src="<?php echo Product::getImage($recProduct['id']); ?>" alt="" />
                                            </a>
                                        </div>
                                        
                                        <div class="carousel_middle">
                                        <h2><?php echo $recProduct['price']; ?> Руб.</h2>
                                        <p>
                                            <a href="/product/<?php echo $recProduct['id']; ?>">
                                                <?php echo $recProduct['name']; ?>
                                            </a>
                                        </p>
                                        </div>
                                        
                                        <div class="carousel_bottom">
                                        <a href="#" 
                                           data-id="<?php echo $recProduct['id']; ?>"
                                           class="btn btn-default add-to-cart">
                                            <i class="fa fa-shopping-cart">
                                            </i>В корзину
                                        </a>
                                        </div>    
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    <?php 
                    //После вывода 3 блока нужно закрыть элемент <div class="item active">
                    if ($i == 2){echo '</div><!--/item active-->';} ?>
                        
                    <?php 
                    //Каждые 3 блока, начиная с 6 требуется закрыть элемент <div class="item">
                    if (($i > 3) and (($i + 1) % 3 == 0)) {echo '</div><!--/class="item"-->';} ?>
                    
                        <?php endfor; ?>
                </div><!--/class="item"-->
                    <!--Вывод данных в карусель-->
                </div>
                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>			
            </div>
        </div>

    </div>
</div>
</div>
</section>

<?php include(ROOT.'/views/layouts/footer.php') ?>
        


