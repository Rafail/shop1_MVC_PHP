</div><!--wraper-->
<footer id="footer"><!--Footer-->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2016</p>
                <p class="pull-right">Тестовый интернет магазин</p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->



    <script src="/template/js/jquery.js"></script>
    <script src="/template/js/bootstrap.min.js"></script>
    <script src="/template/js/jquery.scrollUp.min.js"></script>
    <script src="/template/js/price-range.js"></script>
    <script src="/template/js/jquery.prettyPhoto.js"></script>
    <script src="/template/js/main.js"></script>
    <!--Добавление в корзину используя Ajax запрос-->
    <script>
        $(document).ready(function(){
            //создание события для кнопки "В корзину"
            $(".add-to-cart").click(function(){
                //сохранение значения id товара
                var id = $(this).attr("data-id");
                //проверка на существование элемента Input c #amount
                if($('input').is('#amount')){
                //если элемент существует, сохраняется его значение
                var amount = $("#amount").val();
                } else {
                //иначе присваивается минимальное значение 1 
                var amount = 1;
                }
                //Отправка Ajax запроса с id товара и количеством
                $.post("/cart/addAjax/"+id+"/"+amount, {}, function (data){
                    //Общее количество товара возвращается в header
                    $("#cart-count").html(data);
                });
                return false;
            });
        });
    </script>
</body>
</html>
