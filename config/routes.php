<?php
//Файл для хранения маршрутов

return array(
    //запрос в адресной строке => маршрут обработки запроса Контроллер/Метод/параметры
    //Просмотр одного товара по id
    'product/([0-9]+)' => 'product/view/$1',                        //Маршрут actionView с передачей id продукта в ProductController
    //Просмотр каталога всех товаров (если status = 1)
    'catalog' => 'catalog/index',                                   //Маршрут actionIndex в CatalogController
    //Категория товара:
    'category/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2',  //Маршрут actionCategory с передачей id категории и номера страницы в CategoryController 
    'category/([0-9]+)' => 'catalog/category/$1',                   //Маршрут actionCategory с передачей id категории в CategoryController
    //Корзина:
    'cart/addAjax/([0-9]+)/([0-9]+)' => 'cart/addAjax/$1/$2',
    'cart/delete/([0-9]+)' => 'cart/delete/$1',
    'cart/checkout' => 'cart/checkout',
    'cart' => 'cart/index',
    //Управление товарами:
    'admin/product/create' => 'adminProduct/create',
    'admin/product/update/([0-9]+)' => 'adminProduct/update/$1',
    'admin/product/delete/([0-9]+)' => 'adminProduct/delete/$1',
    'admin/product' => 'adminProduct/index',
    //Управление категориями:
    'admin/category/create' => 'adminCategory/create',
    'admin/category/update/([0-9]+)' => 'adminCategory/update/$1',
    'admin/category/delete/([0-9]+)' => 'adminCategory/delete/$1',
    'admin/category' => 'adminCategory/index',
    //Управление заказами:
    'admin/order/update/([0-9]+)' => 'adminOrder/update/$1',
    'admin/order/delete/([0-9]+)' => 'adminOrder/delete/$1',
    'admin/order/view/([0-9]+)' => 'adminOrder/view/$1',
    'admin/order' => 'adminOrder/index',
    //Панель администратора
    'admin' => 'admin/index',
    //Пользователь:
    'user/register' => 'user/register',                             //Маршрут actionRegister в UserController
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',
    'cabinet/history/update/([0-9]+)' => 'userCabinet/update/$1',
    'cabinet/history/delete/([0-9]+)' => 'userCabinet/delete/$1',
    'cabinet/history/view/([0-9]+)' => 'userCabinet/view/$1',
    'cabinet/history' => 'userCabinet/history',
    'cabinet/edit' => 'cabinet/edit',
    'cabinet' => 'cabinet/index',
    //О магазине
    'contacts' => 'site/contact',
    //Главная страница
    'page-([0-9]+)' => 'site/index/$1',                             //Маршрут actionIndex в SiteController Главная страница с указанием страницы
    '' => 'site/index',                                             //Маршрут actionIndex в SiteController Главная страница
);
