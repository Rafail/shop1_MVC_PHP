<?php

/**
 * Контроллер управления заказами в личном кабинете пользователя
 */
class UserCabinetController extends UserBase {
    /**
     * Action для страницы "Список покупок" в Личном кабинете
     */
    public function actionHistory()
    {
        // Проверка доступа и передача id пользователя
        $userId = self::checkUser();

        // Получаем список заказов
        $ordersList = Order::getOrdersListByUserId($userId);

        // Подключаем вид
        require_once(ROOT . '/views/cabinet/order_list.php');
        return true;
    }

    /**
     * Action для страницы "Редактирование заказа"
     */
    public function actionUpdate($id){
        // Проверка доступа
        self::checkUser();

        // Получаем данные о конкретном заказе
        $order = Order::getOrderById($id);

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена   
            // Получаем данные из формы
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];

            // Сохраняем изменения
            Order::updateOrderById($id, $userName, $userPhone, $userComment);

            // Перенаправляем пользователя на страницу управлениями заказами
            header("Location: /cabinet/history");
        }

        // Подключаем вид
        require_once(ROOT . '/views/cabinet/update.php');
        return true;
    }

    /**
     * Action для страницы "Просмотр заказа"
     */
    public function actionView($id){
        // Проверка доступа
        self::checkUser();

        // Получаем данные о конкретном заказе
        $order = Order::getOrderById($id);

        // Получаем массив с идентификаторами и количеством товаров
        $productsQuantity = json_decode($order['products'], true);

        // Получаем массив с индентификаторами товаров
        $productsIds = array_keys($productsQuantity);

        // Получаем список товаров в заказе
        $products = Product::getProductsByIds($productsIds);

        // Подключаем вид
        require_once(ROOT . '/views/cabinet/view.php');
        return true;
    }

    /**
     * Action для страницы "Удалить заказ"
     */
    public function actionDelete($id){
        // Проверка доступа
        self::checkUser();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем заказ
            Order::deleteOrderById($id);

            // Перенаправляем пользователя на страницу управлениями товарами
            header("Location: /cabinet/history");
        }

        // Подключаем вид
        require_once(ROOT . '/views/cabinet/delete.php');
        return true;
    }
}
