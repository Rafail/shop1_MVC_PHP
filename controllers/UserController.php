<?php

class UserController {
    
    
    public function actionRegister(){
        
        $name = '';
        $email = '';
        $password = '';
        
        if (isset($_POST['submit'])){
            $name = trim($_POST['name']);
            $email = trim($_POST['email']);
            $password = trim($_POST['password']);
            
            //Переменная для хранения ошибок, если данные введены не правильно
            $errors = FALSE;
            
            if (!User::checkName($name)){
                $errors[] = 'Имя не должно быть короче 2 символов!';
            }
            
            if (!User::checkEmail($email)){
                $errors[] = 'Адрес электронной почты не верный!';
            }
            
            if (!User::checkPassword($password)){
                $errors[] = 'Пароль должен состоять минимум из 6 символов!';
            }
            
            if (!User::checkEmailExists($email)){
                $errors[] = 'Такой адрес электронной почты уже зарегистрирован!';
            }
            
            if ($errors == FALSE){
                $result = User::register($name, $email,$password);
                
                $userId = User::checkUserData($email, $password);
                User::auth($userId);
                
                //Перенаправляем пользователя в закрытую часть - кабинет
                header("Location: /cabinet/");
            }
        }
        
        require_once (ROOT.'/views/user/register.php');
    
        return true;
    }
    
    public function actionLogin(){
        
        $email = '';
        $password = '';
        
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            
            $errors = FALSE;
            
            //Валидация полей
            if (!User::checkEmail($email)){
                $errors[] = 'Указан неправильный адрес элетронной почты';
            }
            if (!User::checkPassword($password)){
                $errors[] = 'Пароль не может быть короче 6 символов';
            }
            
            $userId = User::checkUserData($email, $password);
            
            //Проверка на существование пользователя
            if ($userId == FALSE){
                $errors[] = 'Неправильные данные для входа на сайт';
            } else {
                //Если данные правильные, запоминаем пользователя (сессия)
                User::auth($userId);
                
                //Перенаправляем пользователя в закрытую часть - кабинет
                header("Location: /cabinet/");
            }
        }
        
        require_once ROOT.'/views/user/login.php';
        
        return TRUE;
    }
    
    public function actionLogout(){
        
        unset($_SESSION['user']);
        header("Location: /");
    }
}
