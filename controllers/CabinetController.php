<?php

class CabinetController {
    
    public function actionIndex(){
        
        $userId = User::checkLogged();
        
        $user = User::getUserById($userId);
        
        require_once(ROOT.'/views/cabinet/index.php');
        
        return TRUE;
    }
    
    public function actionEdit(){
        //Получение id пользователя из сессии
        $userId = User::checkLogged();
        
        //Получение информации о пользователе из БД
        $user = User::getUserById($userId);
        
        $name = $user['name'];
        $password = $user['password'];
        
        $result = FALSE;
        
        if (isset($_POST['submit'])){
            $name = $_POST['name'];
            $password = $_POST['password'];
            
            $errors = FALSE;
        
            if (!User::checkName($name)){
                $errors[] = 'Имя не должно быть короче 2 символов';
            }

            if (!User::checkPassword($password)){
                $errors[] = 'Пароль не может быть короче 6 символов';
            }

            if ($errors == FALSE){
                $result = User::edit($userId, $name, $password);
            }
        }
        
        require_once ROOT.'/views/cabinet/edit.php';
        
        return TRUE;
    }
}
