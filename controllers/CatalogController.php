<?php

class CatalogController {
    /**
     * Возвращает массив со всеми товарами для Каталога товаров
     * со значением status = 1, те доступными товарами
     * @return boolean
     */
    public function actionIndex() {
        
        $categories = [];
        //вызов метода для категорий товара
        $categories = Category::getCategoriesList();
        
        $totalCount = 0;
        $totalCount = Product::getTotalProducts();
        
        $latestProducts = [];
        $latestProducts = Product::getLatestsProducts($totalCount);
        
        require_once(ROOT.'/views/catalog/index.php');
        
        return TRUE;
    }
    /**
     * Возвращает список товаров по категориями с использованием paginator
     * @param type $categoryId
     * @param type $page
     * @return boolean
     */
    public function actionCategory($categoryId, $page = 1) {
        
        $categories = [];
        //вызов метода для категорий товара
        $categories = Category::getCategoriesList();
        
        $categoryProducts = [];
        $categoryProducts = Product::getProductsListByCategory($categoryId, $page);
        
        $total = Product::getTotalProductsInCategory($categoryId);
        
        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');
        
        require_once(ROOT.'/views/catalog/category.php');
        
        return TRUE;
    }
  
}
