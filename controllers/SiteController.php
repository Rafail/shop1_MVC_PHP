<?php
/**
 * Контроллер Главной страницы
 */
class SiteController {
    // action Главной страницы
    public function actionIndex($page = 1) {
        
        $categories = [];
        //вывод категорий товаров для меню слева
        $categories = Category::getCategoriesList();
        
        $latestProducts = [];
        //вывод Последние товары на Главной странице
        $latestProducts = Product::getLatestsProducts(Product::SHOW_BY_DEFAULT, $page);
        
        $recommendedProducts = [];
        //вывод Рекомендованных товаров для карусели внизу страницы
        $recommendedProducts = Product::getRecommendedProducts();
        //длина массива Рекомендованных товаров
        //используется для правильного вывода товаров в карусели
        $recommendedLenght = count($recommendedProducts);
        
        $total = Product::getTotalProducts();
        //запуск постраничной навигации для Главной страницы
        $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');
        
        require_once(ROOT.'/views/site/index.php');
        
        return TRUE;
    }
    
    /**
     * action обработки отправки письма в разделе "Контакты"
     * @return boolean
     */
    public function actionContact(){
        
        $userEmail = '';
        $userText = '';
        $result = false;
        
        //Если получены данные из формы "Обратная связь"
        if (isset($_POST['userEmail'])) {
            
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];
            
            $errors = FALSE;
            
            //Валидация полей
            if (!User::checkEmail($userEmail)){
                $errors[] = 'Неправильный адрес почты';
            }
            //Если поля прошли валидацию, отправляется письмо
            if ($errors == FALSE) {
                $adminEmail = 'makeitpossible@yandex.ru';
                $message = "Текст: {$userText}. От {$userEmail}";
                $subject = 'Тема письма';
                $result = mail($adminEmail, $subject, $message);
                $result = true;
            }
        }
        
        require_once ROOT.'/views/site/contact.php';
        
        return TRUE;
    }
}

