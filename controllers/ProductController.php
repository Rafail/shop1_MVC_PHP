<?php
//Контроллер для обработки товара
class ProductController {
    /**
     * action обработки товара по id
     * возвращает массив с данными о товаре
     * @param type $productId = INT
     * @return boolean
     */
    public function actionView($productId) {
        
        $categories = array();
        //вызов метода для категорий товара
        $categories = Category::getCategoriesList();
        
        //вызов метода для выборки товара по Id
        $product = Product::getProductById($productId);
        
        require_once(ROOT.'/views/product/view.php');
        
        return TRUE;
    }
}

