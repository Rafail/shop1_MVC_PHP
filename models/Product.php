<?php

class Product {
    //задание константы для вывод количества товаров по умолчанию
    const SHOW_BY_DEFAULT = 6;
    /**
     * Метод возвращает массив товаров с учетом страницы Paginator
     * @param type $count = INT
     * @param type $page = INT
     * @return type = array
     */
    public static function getLatestsProducts($count = self::SHOW_BY_DEFAULT, $page = 1) {
        
        $count = intval($count);
        $page = intval($page);
        //задание начального значения сдвига выдачи результата из БД
        //необходимо для правильного отображения товаров
        //при использовании Paginator
        $offset = ($page - 1) * $count;
        
        //подключение к БД
        $db = Db::getConnection();
        
        $productsList = array();
        
        //отправка запроса в БД
        $result = $db->query('SELECT id, name, price, is_new FROM product '
                . 'WHERE status = "1"'
                . 'ORDER BY id DESC '
                . 'LIMIT ' . $count
                . ' OFFSET '. $offset);
        
        $i = 0;
        //Создание ассоциированного массива Последних товаров
        while ($row = $result->fetch()){
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            //для цены применяется форматирование вида 10000 => 10 000
            $productsList[$i]['price'] = Cart::getRuNumberFormat($productsList[$i]['price']);
            $productsList[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        //Метод возвращает массив товаров с учетом страницы Paginator
        return $productsList;
    }
    
    /**
     * Метод возвращает массив Рекомендованных товаров
     * @return type = array
     */
    public static function getRecommendedProducts(){
        //подключение к БД
        $db = Db::getConnection();
        
        $productsList = array();
        
        //отправка запроса в БД
        $result = $db->query('SELECT id, name, price, is_new FROM product '
                . 'WHERE status = "1" AND is_recommended = "1"');
        
        $i = 0;
        //Создание ассоциированного массива Рекомендованных товаров
        while ($row = $result->fetch()){
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            //для цены применяется форматирование вида 10000 => 10 000
            $productsList[$i]['price'] = Cart::getRuNumberFormat($productsList[$i]['price']);
            $productsList[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        //Метод возвращает массив Рекомендованных товаров
        return $productsList;
    }
    
    /**
     * Метод возвращает массив товаров по id Категории
     * с учетом страницы Paginator
     * @param type $categoryId = INT
     * @param type $page = INT
     * @return type = array
     */
    public static function getProductsListByCategory($categoryId = FALSE, $page = 1){
        //Метод работает, только если было передано Id категории
        if ($categoryId) {
            
            $page = intval($page);
            $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
            
            //подключение к БД
            $db = Db::getConnection();
            
            $products = array();
            
            //отправка запроса в БД
            $result = $db->query("SELECT id, name, price, is_new FROM product "
                . "WHERE status = '1' AND Category_id = '$categoryId' " 
                . "ORDER BY id DESC "
                . "LIMIT ".self::SHOW_BY_DEFAULT
                . " OFFSET ". $offset);
        
        $i = 0;
        
        //Создание ассоциированного массива конкретной Категории товаров
        while ($row = $result->fetch()){
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            //для цены применяется форматирование вида 10000 => 10 000
            $products[$i]['price'] = Cart::getRuNumberFormat($products[$i]['price']);
            $products[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        //Метод возвращает массив товаров по id Категории
        //с учетом страницы Paginator
        return $products;
        }
    }
    
    /**
     * Метод возвращает массив с данными о Товаре по id
     * @param type $id = INT
     * @return type = array
     */
    public static function getProductById($id){
        
        $id = intval($id);
        
        if($id){
            $db = Db::getConnection();
            
            $result = $db->query("SELECT * FROM product WHERE id= '$id'");
            //Настройка выводов результата в виде ассоциированного массива
            $result->setFetchMode(PDO::FETCH_ASSOC);
            
            $product = $result->fetch();
            
            //Метод возвращает массив с данными о Товаре по id
            return $product;
        }
    }
    
    public static function getProductsByIds($idsArray){
        $products = [];
        
        $db = Db::getConnection();
        
        $idsString = implode(',', $idsArray);
        
        $sql = "SELECT * FROM product WHERE status = '1' AND id IN ($idsString)";
        
        $result = $db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        $i = 0;
        while ($row = $result->fetch()){
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            //для цены применяется форматирование вида 10000 => 10 000
            $products[$i]['price'] = Cart::getRuNumberFormat($products[$i]['price']);
            $i++;
        }
        
        return $products;
    }

        public static function getTotalProducts(){
        //подключение к БД
        $db = Db::getConnection();
        
        $result = $db->query("SELECT count(id) AS count FROM product "
                . "WHERE status = '1'");
        
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        $row = $result->fetch();
        
        return $row['count'];
    }
    
    public static function getTotalProductsInCategory($categoryId){
        //подключение к БД
        $db = Db::getConnection();
        
        $result = $db->query("SELECT count(id) AS count FROM product "
                . "WHERE status = '1' AND category_id = '$categoryId'");
        
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        $row = $result->fetch();
        
        return $row['count'];
    }
    
    /**
     * 
     * @return type = array
     */
    public static function getProductsList(){
        //подключение к БД
        $db = Db::getConnection();
        
        $result = $db->query('SELECT id, name, price, code FROM product '
                . 'ORDER BY id ASC');
        
        $productsList = [];
        $i = 0;
        while ($row = $result->fetch()){
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['price'] = $row['price'];
            $i++;
        }
        return $productsList;
    }
    
    /**
     * Удаление товара по id в Админпанели
     * @param type $id = INT
     * @return type = array
     */
    public static function deleteProductById($id){
        //подключение к БД
        $db = Db::getConnection();
        
        $sql = 'DELETE FROM product WHERE id = :id';
        
        $result = $db->prepare($sql);
        $result->bindValue(':id', $id, PDO::PARAM_INT);
        
        return $result->execute();
    }
    
    /**
     * Добавляет новый товар в Админпанели
     * @param array $options <p>Массив с информацией о товаре</p>
     * @return integer <p>id добавленной в таблицу записи</p>
     */
    public static function createProduct($options){
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = 'INSERT INTO product '
                . '(name, code, price, category_id, brand, availability,'
                . 'description, is_new, is_recommended, status)'
                . 'VALUES '
                . '(:name, :code, :price, :category_id, :brand, :availability,'
                . ':description, :is_new, :is_recommended, :status)';
        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи
            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }
    
    /**
     * Редактирует товар с заданным id в Админпанели
     * @param integer $id <p>id товара</p>
     * @param array $options <p>Массив с информацей о товаре</p>
     * @return boolean <p>Результат выполнения метода</p>
     */
    public static function updateProductById($id, $options){
        // Соединение с БД
        $db = Db::getConnection();
        // Текст запроса к БД
        $sql = "UPDATE product
            SET 
                name = :name, 
                code = :code, 
                price = :price, 
                category_id = :category_id, 
                brand = :brand, 
                availability = :availability, 
                description = :description, 
                is_new = :is_new, 
                is_recommended = :is_recommended, 
                status = :status
            WHERE id = :id";
        
        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }
    
    /**
     * Возвращает путь к изображению
     * @param integer $id
     * @return string <p>Путь к изображению</p>
     */
    public static function getImage($id)
    {
        // Название изображения-пустышки
        $noImage = 'no-image.jpg';
        // Путь к папке с товарами
        $path = '/upload/images/products/';
        // Путь к изображению товара
        $pathToProductImage = $path . $id . '.jpg';
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            // Если изображение для товара существует
            // Возвращается путь изображения товара
            return $pathToProductImage;
        }
        // Иначе возвращается путь изображения-пустышки
        return $path . $noImage;
    }
}

