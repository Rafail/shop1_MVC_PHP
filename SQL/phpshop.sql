-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 06 2016 г., 13:35
-- Версия сервера: 5.5.50
-- Версия PHP: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `phpshop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Ноутбуки', 1, 1),
(2, 'Компьютеры', 2, 1),
(3, 'Мониторы', 3, 1),
(4, 'Смартфоны', 5, 1),
(5, 'Планшеты', 4, 1),
(6, 'Игровые приставки', 10, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `price` float NOT NULL,
  `availability` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_new` int(11) NOT NULL DEFAULT '0',
  `is_recommended` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `code`, `price`, `availability`, `brand`, `description`, `is_new`, `is_recommended`, `status`) VALUES
(34, 'Ноутбук Asus X555UJ 90NB0AG2-M00960', 1, 171998, 44990, 1, 'Asus', 'Диагональ 15.6 " \r\nПроцессор Intel Core i7-6500U 3.10 GHz\r\nЖесткий диск 500 Gb HDD\r\nПамять 4 Gb DDR3 1600 Mhz\r\nВидеокарта NVIDIA GeForce 920M\r\nОС Windows 10 \r\nWiFi 802.11b/g/n\r\nBluetooth Да\r\nВес 2.3 кг', 1, 1, 1),
(58, 'Ноутбук Lenovo IdeaPad 100-15IBY N2840', 1, 170794, 16780, 1, 'Lenovo', 'Диагональ 15.6"\r\nПроцессор Intel Celeron N2840 2.16 GHz\r\nЖесткий диск 500 Gb HDD\r\nПамять 2 Gb DDR3 1600Mhz\r\nВидеокарта Intel HD Graphics\r\nОС Windows 10\r\nWiFi 802.11b/g/n\r\nBluetooth Да\r\nВес 2.3 кг', 0, 0, 1),
(59, 'Ноутбук Apple MacBook Air MJVM2RU / A', 1, 138242, 63500, 1, 'Apple', 'Диагональ11.6"\r\nПроцессор Intel Core i5 1.60 GHz\r\nПамять 4 Gb DDR3 1600Mhz\r\nВидеокарта Intel HD Graphics 5000\r\nОС Mac OS X\r\nWiFi 802.11b/g/n/ac\r\nBluetooth Да\r\nВес 1.35 кг', 1, 1, 1),
(60, 'Ноутбук Asus X556UB 90NB09R1-M00470', 1, 172063, 50970, 1, 'Asus', 'Диагональ 15.6"\r\nПроцессор Intel Core i7-6500U 3.10 GHz\r\nЖесткий диск 1000 Gb HDD\r\nПамять 8 Gb DDR3 1600Mhz\r\nВидеокарта NVIDIA GeForce 940M\r\nОС Windows 10\r\nWiFi 802.11b/g/n\r\nBluetooth Да\r\nВес 2.2 кг', 0, 0, 1),
(61, 'Ноутбук Dell Inspiron 3542 3542-7791', 1, 164976, 17870, 1, 'Dell', 'Диагональ 15.6"\r\nПроцессор Intel Celeron 2957 U\r\nЖесткий диск 500 Gb HDD\r\nПамять 2 Gb DDR3 1600Mhz\r\nВидеокарта Intel HD Graphics\r\nОС Windows 10\r\nWiFi Да\r\nBluetooth Да\r\nВес 2.4 кг', 0, 1, 1),
(62, 'Ноутбук HP Pavilion 15-ab132ur V0Z42EA', 1, 173540, 47330, 1, 'HP', 'Диагональ15.6"\r\nПроцессор AMD A10-8780P 2.0 GHz\r\nЖесткий диск 1000 Gb HDD\r\nПамять 6 Gb DDR3 1600Mhz\r\nВидеокарта AMD Radeon R7 M360\r\nОС Windows 10\r\nWiFi 802.11b/g/n\r\nBluetooth Да\r\nВес 2.29 кг', 0, 1, 1),
(63, 'Ультрабук Lenovo IdeaPad Yoga 510 80S7005ERK', 1, 178339, 47350, 1, 'Lenovo', 'Диагональ 14"\r\nПроцессор Intel Core i3-6100U 2.30 GHz\r\nЖесткий диск 1000 Gb HDD\r\nПамять 4 Gb DDR3\r\nВидеокарта Intel HD Graphics 520\r\nОС Windows 10\r\nWiFi 802.11b/g/n/ac\r\nBluetooth Да\r\nВес 1.8 кг', 0, 1, 1),
(64, 'Ноутбук HP Probook 430', 1, 170906, 49480, 1, 'HP', 'Диагональ 13.3"\r\nПроцессор Intel Core i3-6100U 2.30 GHz\r\nЖесткий диск Нет\r\nПамять 4 Gb DDR3L 1600Mhz\r\nВидеокарта Intel HD Graphics 520\r\nОС Windows 7 Professional\r\nWiFi Да\r\nBluetooth Да\r\nВес 1.5 кг', 0, 0, 1),
(65, 'Ноутбук HP Zbook 17 K0G78ES', 1, 173790, 198190, 1, 'HP', 'Диагональ 17.3"\r\nПроцессор Intel Core i7-4710MQ 2.50 GHz\r\nЖесткий диск 750 Gb HDD\r\nПамять16 Gb DDR3L 1600Mhz\r\nВидеокарта NVIDIA Quadro K4100M\r\nОС Windows 7 Professional 64\r\nWiFi 802.11b/g/n/ac\r\nBluetooth Да\r\nВес 3.41 кг', 0, 1, 1),
(66, 'Ноутбук HP ZBook 17 K0G82ES', 1, 168946, 293390, 1, 'HP', 'Диагональ 17.3"\r\nПроцессор Intel Core i7-4940 MX Extreme Edition 4.00 GHz\r\nЖесткий диск 1000 Gb HDD\r\nПамять 32 Gb DDR3L 1600Mhz\r\nВидеокарта NVIDIA Quadro K2200M\r\nОС Windows 7 Professional\r\nWiFi Да\r\nBluetooth Да\r\nВес 3.41 кг', 1, 0, 1),
(67, 'Ноутбук Apple MacBook Pro MF840RU / A', 1, 138247, 109500, 1, 'Apple', 'Диагональ 13.3"\r\nПроцессор Intel Core i5 2.70 GHz\r\nПамять 8 Gb DDR3 1866Mhz\r\nВидеокарта Intel Iris Graphics 6100\r\nОС Mac OS X\r\nWiFi 802.11b/g/n/ac\r\nBluetooth Да\r\nВес 1.57 кг', 0, 0, 1),
(68, 'Ноутбук HP Spectre x360 G2 V1B05EA', 1, 173741, 109790, 1, 'HP', 'Диагональ 13.3"\r\nПроцессор Intel Core i5-6200U 2.80 GHz\r\nЖесткий диск Нет\r\nПамять 8 Gb DDR3L 1600Mhz\r\nВидеокарта Intel HD Graphics 520\r\nОС Windows 10 Professional 64\r\nWiFi 802.11b/g/n/ac\r\nBluetooth Да\r\nВес 1.48 кг', 0, 0, 1),
(69, 'Ноутбук HP EliteBook Folio G1 V1C40EA', 1, 175249, 110470, 1, 'HP', 'Диагональ 12.5"\r\nПроцессор Intel Core M5-6Y54 2.70 GHz\r\nЖесткий диск Нет\r\nПамять 8 Gb DDR3 1866Mhz\r\nВидеокарта Intel HD Graphics 515\r\nОС Windows 7 Professional 64\r\nWiFi 802.11b/g/n/ac\r\nBluetooth Да\r\nВес 0.97 кг', 0, 0, 1),
(70, 'Ноутбук HP EliteBook 850 G3 T9X56EA', 1, 173767, 113530, 0, 'HP', 'Диагональ 15.6"\r\nПроцессор Intel Core i7-6500U 3.10 GHz\r\nЖесткий диск Нет\r\nПамять 8 Gb DDR4 2133Mhz\r\nВидеокарта Intel HD Graphics 520\r\nОС Windows 7 Professional 64\r\nWiFi 802.11b/g/n/ac\r\nBluetooth Да\r\nВес 1.88 кг', 0, 0, 1),
(71, 'Компьютер HP EliteDesk 800 G1 USDT', 2, 162722, 34170, 1, 'HP', 'Процессор Intel Pentium G3250 (3M Cache, 3.20 GHz)\r\nПамять 4гб DDR3 1600 МГц\r\nЖёсткий диск 500гб 7200об./мин\r\nВидеокарта Intel HD Graphics\r\nОС DOS\r\nWiFi Нет\r\nBluetooth Нет\r\nHDMI Нет\r\n', 1, 1, 1),
(72, 'Компьютер HP ProDesk 600 G1 J7C46EA', 2, 162740, 41790, 0, 'HP', 'Процессор Intel Core i3 4160 (2 ядра, 3.6 GHz)\r\nПамять 4гб DDR3 1600 МГц\r\nЖёсткий диск 500гб 7200об./мин\r\nВидеокарта Intel HD Graphics 4400\r\nОС Microsoft Windows 7 Professional 64\r\nWiFi Нет\r\nBluetooth Нет\r\nHDMI Нет', 0, 0, 1),
(73, 'Игровая приставка Sony PS4 500Gb Black (CUH-1208A)', 6, 82798, 27890, 1, 'Sony', 'Комплектация:\r\nКабель USB, кабель для цифр.подкл. (HDMI), гарнитура, джойстик, инструкция, гар. талон.', 0, 0, 1),
(74, 'Игровая приставка Microsoft Xbox 360 E 500GB + Forza Horizon + Forza Horizon 2', 6, 164822, 11190, 1, 'Microsoft', 'Комплектация	Гарнитура, геймпад, инструкция, Forza Horizon + Forza Horizon 2', 0, 0, 1),
(75, 'Игровая приставка Microsoft Xbox One 1Tb HDD / SSD Hybrid + Elite Gamepad', 6, 165326, 32490, 1, 'Microsoft', '', 1, 1, 1),
(76, 'Планшет Lenovo IdeaPad Tab2 ZA0D0053RU', 5, 168081, 12950, 1, 'Lenovo', 'Размер экрана 10.1"\r\nРазрешение 1280 x 720\r\nТип матрицы IPS\r\nGPS-приемник Да\r\nВстроенная память (Гб)16\r\nОС Android 5.0 кол-во ядер 4\r\n3G Да', 0, 0, 1),
(77, 'Планшет Lenovo YT3-850M LTE', 5, 157162, 14475, 1, 'Lenovo', 'Размер экрана 8"\r\nРазрешение 1280 x 800\r\nТип матрицы IPS\r\nВстроенная память (Гб) 16\r\nОС Android 5.1 кол-во ядер 4\r\n3G Да', 0, 1, 1),
(78, 'Планшет Huawei MediaPad T1 10 LTE', 5, 160502, 14900, 1, 'Huawei', 'Размер экрана 9.6"\r\nРазрешение 1280 x 800\r\nТип матрицы IPS\r\nGPS-приемник Да\r\nВстроенная память (Гб)16\r\nОС Android 4.4 кол-во ядер 4\r\n3G Да', 1, 1, 1),
(79, 'Планшет Asus ZenPad Z300CG White 90NP0213-M00710', 5, 150519, 15090, 1, 'Asus', 'Размер экрана 10.1"\r\nРазрешение 1280 x 800\r\nТип матрицы IPS\r\nGPS-приемник Да\r\nВстроенная память (Гб)16\r\nОС Android 5.0\r\nкол-во ядер 4\r\n3G Да', 0, 0, 1),
(80, 'Планшет Apple iPad mini 4 64Gb WiFi Space Gray (MK9G2RU / A)', 5, 158315, 38799, 1, 'Apple', 'Краткие технические характеристики\r\nПартномер	MK9G2RU/A\r\nПроизводитель	Apple\r\nLTE(4G)	Нет\r\nАртикул	158315\r\nОперационная система	iOS\r\nПроцессор	Apple A8 (1.5 Ггц)\r\nВстроенная память (Гб)	64\r\nWiFi	Да\r\nBluetooth	Да\r\nРазъём SIM карты	Нет\r\nЦвет	Серый\r\nВес (гр.)	299\r\nБатарея	Li-ion 5124 мАч\r\n3G	Нет\r\nДиагональ "	7.9', 0, 1, 1),
(81, 'Смартфон Sony D5322 Xperia T2 Ultra Dual White', 4, 103089, 15490, 0, 'Sony', 'Вес 172 гр\r\nВремя работы в режиме ожидания 1046 ч. (2G), до 1071 ч. (3G)\r\nBluetooth Да\r\nWiFi Да\r\n3G Да\r\nПоддержка двух SIM-карт Да\r\nВстроенный GPS приемник Да\r\nОперационная система/платформа Android 4.3\r\nРазрешение 1280 x 720\r\nГарантия12 мес.', 0, 0, 1),
(82, 'Смартфон Sony E2312 Xperia M4 Aqua Dual 3G Coral', 4, 152720, 15900, 1, 'Sony', 'Вес 140 гр\r\nВремя работы в режиме ожидания до 779 ч\r\nBluetooth Да\r\nWiFi Да\r\n3G Да\r\nПоддержка двух SIM-карт Да\r\nВстроенный GPS приемник Да\r\nОперационная система/платформа Android 5.0\r\nРазрешение1280 x 720\r\nГарантия 12 мес.', 0, 0, 1),
(83, 'Смартфон ASUS ZenFone Selfie ZD551KL 90AZ00U8-M01270', 4, 159143, 16190, 1, 'Asus', 'Вес 170 гр\r\nBluetooth Да\r\nWiFi Да\r\n3G Да\r\nПоддержка двух SIM-карт Да\r\nВстроенный GPS приемник Да\r\nОперационная система/платформа Android 5.0\r\nРазрешение 1920 x 1080\r\nГарантия 12 мес.', 1, 0, 1),
(84, 'Смартфон Sony D5803 Xperia Z3 compact', 4, 121422, 21390, 1, 'Sony', 'Вес 129 гр\r\nВремя работы в режиме ожидания до 920 ч\r\nBluetooth Да\r\nWiFi Да\r\n3G Да\r\nПоддержка двух SIM-карт Нет\r\nВстроенный GPS приемник Да\r\nОперационная система/платформаAndroid 4.4\r\nРазрешение 1280 x 720\r\nГарантия 12 мес.', 0, 0, 1),
(85, 'Монитор ASUS VX238H 23"', 3, 64869, 11930, 1, 'Asus', 'Размер экрана 23"\r\nТип TFT ЖК-матрицы TN\r\nРазрешение 1920x1080\r\nПодсветка Светодиодная\r\nСоотношение сторон16 : 9\r\nЯркость (кд/м2) 250кд/м2\r\nВстроенные динамики Да\r\nWEB камера Нет\r\nТВ-тюнер Нет\r\nГарантия 36 мес.', 0, 0, 1),
(86, 'Монитор 29" Dell U2913WM', 3, 65723, 35020, 1, 'Dell', 'Размер экрана 29"\r\nТип TFT ЖК-матрицы AH-IPS\r\nРазрешение 2560x1080\r\nПодсветка Светодиодная\r\nСоотношение сторон 21 : 9\r\nЯркость (кд/м2) 300кд/м2\r\nВстроенные динамики Да\r\nWEB камера Нет\r\nТВ-тюнер Нет\r\nГарантия 36 мес.', 1, 0, 1),
(87, 'Монитор ASUS VX239H', 3, 83771, 13410, 0, 'Asus', 'Размер экрана 23"\r\nТип TFT ЖК-матрицы AH-IPS\r\nРазрешение 1920x1080\r\nПодсветка Светодиодная\r\nСоотношение сторон16 : 9\r\nЯркость (кд/м2)250кд/м2\r\nВстроенные динамики Да\r\nWEB камера Нет\r\nГарантия 36 мес.', 0, 0, 1),
(88, 'Монитор 32" Benq BL3200PT', 3, 108442, 35720, 1, 'Benq', 'Размер экрана 32"\r\nРазрешение 2560x1440\r\nПодсветка Светодиодная\r\nСоотношение сторон 16 : 9\r\nЯркость (кд/м2) 300кд/м2\r\nВстроенные динамики Да\r\nWEB камера Нет\r\nТВ-тюнер Нет\r\nГарантия 24 мес.', 0, 0, 1),
(89, 'Монитор 24" LG 24MP58VQ-P', 3, 176494, 11330, 1, 'LG', 'Размер экрана 24"\r\nТип TFT ЖК-матрицы IPS\r\nРазрешение 1920x1080\r\nПодсветка Светодиодная\r\nСоотношение сторон 16 : 9\r\nЯркость (кд/м2) 250кд/м2\r\nГарантия 24 мес.', 0, 0, 1),
(90, 'Монитор Aoc E2270swn', 3, 138943, 5820, 1, 'Aoc', 'Размер экрана 22"\r\nТип TFT ЖК-матрицы TN\r\nРазрешение 1920x1080\r\nПодсветка Светодиодная\r\nСоотношение сторон 16 : 9\r\nЯркость (кд/м2) 200кд/м2\r\nВстроенные динамики Нет\r\nWEB камера Нет\r\nТВ-тюнер Нет\r\nГарантия 36 мес.', 0, 0, 1),
(91, 'Монитор 22" Samsung S24F350FHI', 3, 176318, 10020, 1, 'Samsung', 'Размер экрана 24"\r\nТип TFT ЖК-матрицы PLS\r\nРазрешение 1920x1080\r\nПодсветка Светодиодная\r\nСоотношение сторон 16 : 9\r\nЯркость (кд/м2) 250кд/м2\r\nВстроенные динамики Нет\r\nГарантия 24 мес.', 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_comment` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `products` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_order`
--

INSERT INTO `product_order` (`id`, `user_name`, `user_phone`, `user_comment`, `user_id`, `date`, `products`, `status`) VALUES
(49, 'Вася', '+79250005678', 'Сколько будет стоить доставка?', 9, '2016-08-06 06:45:21', '{"91":1,"61":1,"71":1,"75":1}', 1),
(50, 'Петя', '+79120345256', '', 10, '2016-08-06 06:53:24', '{"80":1,"81":1}', 1),
(51, 'Александр Петрович', '+79036782345', '', 11, '2016-08-06 06:58:46', '{"68":1}', 1),
(52, 'Екатерина Алексеевна', '+79256663311', '', 12, '2016-08-06 07:00:30', '{"80":1,"73":1,"34":1}', 1),
(56, 'admin', '+74995462211', '', 2, '2016-08-06 09:32:33', '{"91":1}', 1),
(57, 'Rafail', '+79250245709', '', 1, '2016-08-06 09:38:51', '{"72":1,"88":1,"74":1}', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`) VALUES
(1, 'Rafail', 'makeitpossible@yandex.ru', '123456', ''),
(2, 'admin', 'admin@shop1.ru', '123456', 'admin'),
(9, 'Вася', 'vasya@mail.ru', '123456', ''),
(10, 'Петя', 'petya@mail.ru', '123456', ''),
(11, 'Александр Петрович', 'petrovich@mail.ru', '123456', ''),
(12, 'Екатерина Алексеевна', 'katia@mail.ru', '123456', '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT для таблицы `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
