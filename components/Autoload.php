<?php
//Автозагрузка классов
function __autoload($class_name)
{
    //Массив хранит имена папок, где храняться подключаемые классы
    $array_paths = array(
        '/models/',
        '/components/'
    );
    
    //Цикл ищет необходимый класс в папках и подключает
    foreach ($array_paths as $path) {
        $path = ROOT . $path . $class_name . '.php';
        if (is_file($path)) {
            include_once $path;
        }
    }
}