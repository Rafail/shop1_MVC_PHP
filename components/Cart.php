<?php

class Cart {
    
    public static function addProduct($id, $amount){
        $id = intval($id);
        $amount = intval($amount);
        
        //Пустой массив для товаров в корзине
        $productsInCart = [];
        
        //Если в корзине уже есть товары (они храняться в сессии)
        if (isset($_SESSION['products'])){
            //То заполняем массив товарами из сессии
            $productsInCart = $_SESSION['products'];
        }
       //Если товар есть в корзине, но был добавлен еще раз, увеличим количество
        if ((array_key_exists($id, $productsInCart)) AND $amount > 1){
            $productsInCart[$id] += $amount;
        } elseif (array_key_exists($id, $productsInCart)){
            $productsInCart[$id] ++;
        } else {
            //Добавляем новый товар в корзину
            $productsInCart[$id] = 1;
        }
        
        $_SESSION['products'] = $productsInCart;
        
        //Возвращение количества товаров, которые находятся в корзине
        return ("(".self::countItems().")");
    }
    //Подсчет количества товаров в корзине (в сессии)
    //return INT
    public static function countItems(){
        if (isset($_SESSION['products'])){
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity){
                $count = $count + $quantity;
            }
            return $count;
        }  else {
            return 0;
        }
    }
    
    public static function getProducts(){
        if(isset($_SESSION['products'])){
            return $_SESSION['products'];
        }
        return FALSE;
    }
    //Преобразования числа в денежный формат
    public static function getRuNumberFormat($number){
        
         return $number = number_format($number, 0, ' , ', ' ');
    }
    //Преобразование денежного формата в число(строку) удалением пробелов
    public static function getNumberFromRuFormat($number){
        
        return preg_replace('/ /', '', $number);
    }
    
    //Подсчет общей цены всех товаров в корзине
    public static function getTotalPrice($products){
        //Получаем товары из сессии
        $productsInCart = self::getProducts();
        
        $total = 0;
        //Если товары есть
        if ($productsInCart){
            foreach ($products as $item){
                $total += self::getNumberFromRuFormat($item['price']) * $productsInCart[$item['id']];
            }
        }
        
        $total = self::getRuNumberFormat($total);
        
        return $total;
    }
    
    //удаление всех товаров из корзины
    public static function clear(){
        if (isset($_SESSION['products'])){
            unset($_SESSION['products']);
        }
    }
    
    //Удаление товара из корзины по id
    public static function deleteProductFromCartById($id){
        if (isset($_SESSION['products'][$id])){
            unset($_SESSION['products'][$id]);
        }
    }
        
}
