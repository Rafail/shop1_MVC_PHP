<?php

class Db {
    
    public static function getConnection() {
        
        //подключение файла с настройками соединения(имя хоста, логин, пароль)
        //путь к файлу
        $paramsPath = ROOT . '/config/db_params.php';
        //подключение файла, получение массива с настройками
        $params = include($paramsPath);
        
        //создание объекта класса PDO
        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);
        
        //Установка кодировки для БД
        $db->exec("set names utf8");
        
        //возвращается объект класса PDO
        return $db;
    }
}

