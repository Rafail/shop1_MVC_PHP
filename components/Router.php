<?php

class Router
{
    //Массив для хранения маршутов
    private $routes;
    //конструктор для чтения маршрутов
    public function __construct() {
        //путь к файлу маршрутов
        $routesPath= ROOT . '/config/routes.php';
        //сохранение массива маршрутов в объекте в переменную $routes
        $this->routes = include($routesPath);
    }
    
    //вспомогательный метод, возвращает строку запроса
    private function getURI() {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    //принимает управление от FrontController
    //запуск обработки запроса
    //передача управления соответствующему Методу Контроллера
    public function run() {
        //Получение строки запроса
        $uri = $this->getURI();
        
        //Проверка наличия этого запроса в ассоциированном массиве маршрутов routes.php
        //где $uriPattern является ключом, а $path значением ключа
        foreach ($this->routes as $uriPattern => $path){
            
            //Сравнение $uriPattern и $uri
            //Если есть совпадение, определить какой контроллер
            // и action обрабатывают запрос
            if (preg_match("~^$uriPattern$~", $uri)) {
                
                //Получение внутреннего маршрута из внешнего запроса
                //Например, для внешнего запроса '/catalog/'
                // будет соответствовать внутренний маршрут 'catalog/index'
                $internalRoute = preg_replace("~^$uriPattern$~", $path, $uri);
                
                //Разбивка внутреннего маршрута на части по '/'
                //получение контроллера и метода - action
                $segments = explode('/', $internalRoute);
                
                
                //Получение полного имени контроллера с Большой буквы вида 'NameController'
                $controllerName = ucfirst(array_shift($segments).'Controller');

                //Получение имени метода - action с Большой буквы вида 'actionName'
                $actionName = 'action'.ucfirst(array_shift($segments));
                
                //Оставшаяся часть внутреннего маршрута сохраняется в переменной
                //Здесь может храниться, например, id продукта или категории
                $parameters = $segments;
                            
                //Составление полного пути к файлу контроллера
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                
                //Проверка на существование файла контроллера
                if (file_exists($controllerFile)) {
                    //если файл существует, контроллер подключается
                    include_once($controllerFile);
                }
        
                //Создание объекта класса полученного контроллера
                $controllerObject = new $controllerName;
                //
//                $result = $controllerObject->$actionName($parameters);
                //Вызов метода/action соответствующего контроллера с передачей параметров
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                
                //Если метод сработал, цикл поиска маршрутов прекращается
                if ($result != NULL) {
                    break;
                }
            }   
        }
    }
}
